Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  
  concern :paths do
    get 'organizations/:group_organization_ids', to: 'organizations#index'
    get 'locations/:group_organization_id', to: 'locations#index'
  end

  namespace :api do
    namespace :v1 do
      namespace :shared do
        concerns :paths
      end
      namespace :exclusive do
        concerns :paths
        post 'model_type_prices/:group_organization_id', to: 'model_type_prices#index'
      end
    end
  end
end
