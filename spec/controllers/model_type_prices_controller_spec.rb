# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Exclusive::ModelTypePricesController, type: :controller do
    
    describe 'GET index' do
        let(:user_client) { create(:user) }

        it 'get all model type prices' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            model_prices = { results: [{ "name": "php-pa",  "value": 21.31},
                                      { "name": "scrum-pa", "value": 66 },
                                      { "name": "kanban-pa", "value": 21.31 }]
                            }
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            response.body = model_prices.to_json
            expect(response.body).to eq(model_prices.to_json)
        end 

        it 'get all model type prices empty' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.body).to eq({ results: []}.to_json)
        end

        it 'get all model type prices success' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response).to  have_http_status(:success)
        end

        it 'get all model type prices code status 200' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.status).to eq(200)
        end

        it 'get all model type prices content type' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.content_type).to eq('application/json')
        end

        it 'get all model type not content type' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.content_type).not_to eq('application/text')
        end

        it 'get all model type not authorized' do
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.body).to eq({ error: "Not Authorized" }.to_json)
        end

        it 'get all model type not authorized status code' do
            post :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_id: 1, 
                model_type_name: 'Show room', base_price: 15 }
            expect(response.status).to eq(401)
        end
    end
end