# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Exclusive::LocationsController, type: :controller do
    
    describe 'GET index' do
        let(:user_client) { create(:user) }

        it 'get all locations by group organization' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            locations = { locations: [
                          { "name": "Goodison Park" },
                          { "name": "Iduna Park" } ]
                        }
            get :index, :format => :json, 
                params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            response.body = locations.to_json
            expect(response.body).to eq(locations.to_json)
        end

        it 'get all locations empty' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.body).to eq({ locations: [] }.to_json)
        end

        it 'get all locations success' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response).to  have_http_status(:success)
        end

        it 'get all locations code status 200' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.status).to eq(200)
        end

        it 'get all locations content type json' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.content_type).to eq('application/json')
        end

        it 'get all locations not content type json' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.content_type).not_to eq('application/text')
        end

        it 'get all locations not authorized' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.body).to eq({ error: "Not Authorized" }.to_json)
        end

        it 'get all locations not authorized code' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.status).to eq(401)
        end
    end
end

RSpec.describe Api::V1::Shared::LocationsController, type: :controller do
    
    describe 'GET index' do
        
        it 'get all locations by group organization' do
            locations = { locations: [
                          { "name": "Goodison Park" },
                          { "name": "Iduna Park" } ]
                        }
            get :index, :format => :json, 
                params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            response.body = locations.to_json
            expect(response.body).to eq(locations.to_json)
        end

        it 'get all locations empty' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.body).to eq({ locations: [] }.to_json)
        end

        it 'get all locations success' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response).to  have_http_status(:success)
        end

        it 'get all locations code status 200' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.status).to eq(200)
        end

        it 'get all locations content type json' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.content_type).to eq('application/json')
        end

        it 'get all locations not content type json' do
            get :index, :format => :json, params: { use_route: :locations_index_path,
                group_organization_id: 1
            }
            expect(response.content_type).not_to eq('application/text')
        end
    end
end