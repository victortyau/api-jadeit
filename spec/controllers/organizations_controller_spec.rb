# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Exclusive::OrganizationsController, type: :controller do
    
    describe 'GET index' do
        
        let(:user_client) { create(:user) }

        it 'get all organization by group organization' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            organizations = { organizations: [
                              { "id": 1, "name": "php-pa"  },
                              { "id": 2, "name": "ruby-pa" } ] 
                            } 
            get :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_ids: "1,2"
            }
            response.body = organizations.to_json
            expect(response.body).to eq(organizations.to_json)
        end

        it 'get all organization empty' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_ids: "1,2"
            }
            expect(response.body).to eq({ organizations: [] }.to_json)
        end

        it 'get all organization success' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response).to  have_http_status(:success)
        end

        it 'get all organization code status 200' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.status).to  eq(200)
        end

        it 'get all organization content type' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).to eq('application/json')
        end

        it 'get all organization not content type' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).not_to eq('application/text')
        end

        it 'get all organization not content type' do
            request.headers['Authorization'] = 'Bearer '+auth_headers(user_client)
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).not_to eq('application/text')
        end

        it 'get all organization not authorization' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.body).to eq({ error: "Not Authorized" }.to_json)
        end

        it 'get all organization not authorization status code' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.status).to eq(401)
        end
    end
end

RSpec.describe Api::V1::Shared::OrganizationsController, type: :controller do
    
    describe 'GET index' do
        
        it 'get all organization by group organization' do
            organizations = { organizations: [
                { "id": 1, "name": "php-pa"  },
                { "id": 2, "name": "ruby-pa" } ] 
              } 
            get :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_ids: "1,2"
            }
            response.body = organizations.to_json
            expect(response.body).to eq(organizations.to_json)
        end

        it 'get all organization empty' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
                group_organization_ids: "1,2"
            }
            expect(response.body).to eq({ organizations: [] }.to_json)
        end

        it 'get all organization success' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response).to  have_http_status(:success)
        end

        it 'get all organization code status 200' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.status).to  eq(200)
        end

        it 'get all organization content type' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).to eq('application/json')
        end

        it 'get all organization not content type' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).not_to eq('application/text')
        end

        it 'get all organization not content type' do
            get :index, :format => :json, params: { use_route: :organizations_index_path,
            group_organization_ids: "1,2" }
            expect(response.content_type).not_to eq('application/text')
        end
    end
end