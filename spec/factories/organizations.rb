FactoryBot.define do
    factory :organization do
        id { 1 }
        name { "fedora-pa" }
        public_name { "fedora panama developers" }
        association :group_organization, factory: :group_organization
        association :pricing_policy, factory: :pricing_policy
        association :type, factory: :type
    end
end
