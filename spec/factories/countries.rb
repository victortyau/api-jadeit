FactoryBot.define do
    factory :country do
        id { 1 }
        name { "Panama" }
        country_code { "PA" }
    end
end
