FactoryBot.define do
    factory :location do
        id { 1 }
        name { "Craven Cottage" }
        address { "Tella Parken Avenue" }
        association :organization, factory: :organization
    end
end