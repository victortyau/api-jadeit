FactoryBot.define do
    factory :group_organization do
        id { 1 }
        name { "floss-pa" }
        code { "osspa" }
        association :country, factory: :country
    end
end
