FactoryBot.define do
  factory :user do
    name { "Victor Tejada Yau" }
    email { "victortyau@gmail.com" }
    password_digest { "Panama@01" }
  end
end
