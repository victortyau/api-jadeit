FactoryBot.define do
    factory :pricing_policy do
        id  { 1 } 
        name { "Flexible" }     
    end
end