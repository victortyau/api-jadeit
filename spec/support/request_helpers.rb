module Request
  module AuthHelpers
    def auth_headers(user_client)
       Knock::AuthToken.new(payload: { sub: user_client.id }).token
    end
  end
end
