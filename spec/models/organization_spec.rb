require 'rails_helper'

RSpec.describe Organization, type: :model do
  let(:current_organization) { build(:organization) }

  it 'has a valid factory' do  
    expect(current_organization).to be_valid
  end
end
