require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:current_location) { build(:location) }

  it 'has a valid factory' do  
    expect(current_location).to be_valid
  end
end
