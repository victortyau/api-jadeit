require 'rails_helper'

RSpec.describe GroupOrganization, type: :model do
  let(:current_group_organization) { build(:group_organization) }

  it 'has a valid factory' do  
    expect(current_group_organization).to be_valid
  end
end
