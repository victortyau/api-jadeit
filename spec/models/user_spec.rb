require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user_client) { create(:user) }

  it 'has a valid factory' do  
    expect(user_client).to be_valid
  end
end
