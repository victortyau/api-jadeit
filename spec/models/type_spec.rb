require 'rails_helper'

RSpec.describe Type, type: :model do
  let(:current_type) { build(:type) }

  it 'has a valid factory' do  
    expect(current_type).to be_valid
  end
end
