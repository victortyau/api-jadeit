require 'rails_helper'

RSpec.describe Country, type: :model do
  let(:current_country) { build(:country) }

  it 'has a valid factory' do  
    expect(current_country).to be_valid
  end
end
