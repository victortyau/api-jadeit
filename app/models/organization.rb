class Organization < ApplicationRecord
  belongs_to :group_organization
  belongs_to :type
  belongs_to :pricing_policy
  has_many :locations
  has_many :children, class_name: 'Organization', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Organization', optional: true
end
