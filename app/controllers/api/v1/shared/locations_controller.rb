class Api::V1::Shared::LocationsController < ApplicationController
   
    def index
        @group_organizations = params[:group_organization_id]
        @locations = Location.joins(:organization => :group_organization)
        .where(organizations: { group_organization: @group_organizations})
        @result = @locations.collect{|location| { name: location.name }  }
        render json: { locations: @result }, status: :ok
    end
end
