class Api::V1::Shared::OrganizationsController < ApplicationController
    
    def index
        @group_organizations = params[:group_organization_ids].split(",")
        @organizations = Organization
        .where(group_organization_id: @group_organizations)
        @result = @organizations.collect{|org| { id: org.id, name: org.name } }
        render json: { organizations: @result }, status: :ok
    end
end
