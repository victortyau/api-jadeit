class Api::V1::Exclusive::ModelTypePricesController < ApplicationController
    before_action :authenticate_user

    def index
        @margin = { 1 => 6.31, 2 => 13, 3 => 51}
        @group_organizations = params[:group_organization_id]
        @model_type_name = params[:model_type_name]
        @base_price = params[:base_price]
        
        @organizations = Organization.select(:name, :pricing_policy_id).joins(:type)
        .where(group_organization_id: @group_organizations)
        .where(types: { name: @model_type_name })

        @result = @organizations.collect{|org| { name: org.name, value: @base_price.to_i + @margin[org.pricing_policy_id] } }
        render json: { results: @result }, status: :ok 
    end
end
