class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :public_name
      t.references :group_organization, foreign_key: true
      t.references :type, foreign_key: true
      t.references :pricing_policy, foreign_key: true
      t.references :parent, index: true
      t.timestamps
    end
  end
end
