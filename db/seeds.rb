# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
type_list = ["Show room", "Service", "Dealer"]

type_list.each do |name|
    Type.create!(name: name)
end

pricing_policy_list = ["Flexible", "Fixed", "Prestige"]

pricing_policy_list.each do |name|
    PricingPolicy.create!(name: name)
end

country_list = [
    ["Panama", "PA"],
    ["Demmark", "DK"],
    ["Switzerland", "CH"]
]

country_list.each do |name, code| 
    Country.create!(name: name, country_code: code)
end

group_organization_list = [
    ["Floss-PA", "OSS-PA", 1],
    ["Agile-PA", "AG-PA", 1],
    ["Ruby-DK", "RB-DK", 2],
    ["Rails-DK", "DHH-DK", 2],
    ["Kicad-CH", "KI-CH", 3],
    ["Centos-CH", "COS-CH", 3],
]

group_organization_list.each do |name, code , country_id|
    GroupOrganization.create!(name: name, code: code, country_id: country_id)
end

organization_list = [
    ["php-pa", "php panama", 1, 1, 1],
    ["ruby-pa", "ruby panama", 1, 2, 2],
    ["fedora-pa", "fedora panama", 1, 3, 3],
    ["ng-pa", "angular panama", 1, 2, 1],
    ["react-pa", "react panama", 1, 3, 2],

    ["scrum-pa", "scrum panama", 2, 1, 3],
    ["kanban-pa", "kanban panama", 2, 1, 1],
    ["lean-pa", "lean panama", 2, 2, 2],
    ["sprint-pa", "sprint panama", 2, 2, 3],
    ["backlog-pa", "backlog panama", 2, 3, 1],

    ["ruby-dev-dk", "ruby developer denmark", 3, 1, 2],
    ["brigade-dk", "Ringe ruby brigade", 3, 1, 3],
    ["poznan-team-dk", "ruby Vandrup team", 3, 2, 1],
    ["taastrup-group-dk", "ruby group taastrup", 3, 2, 2],
    ["fredericia-dev-dk", "fredericia ruby dev", 3, 3, 3],

    ["rails-dev-dk", "rails developer denmark", 4, 3, 1],
    ["odense-team-dk", "odense team", 4, 1, 2],
    ["kolding-brigade-dk", "kolding rails brigade", 4, 1, 3],
    ["herning-group-dk", "herning rails group", 4, 2, 1],
    ["viborg-dev-dk", "viborg rails dev", 4, 2, 2],

    ["kicad-dev-ch", "kicad developers", 5, 3, 3],
    ["kicad-design-ch", "kicad designers", 5, 3, 1],
    ["kicad-amb-ch", "kicad ambassadors", 5, 1, 2],
    ["kicad-writer-ch", "kicad writers", 5, 1, 3],
    ["kicad-circuit-ch", "kicad circuits", 5, 2, 1],

    ["centos-thun-ch", "centos thun group", 6, 2, 2],
    ["centos-geneva-ch", "centos geneva dev", 6, 3, 3],
    ["centos-lugano-ch", "centos lugano team", 6, 3, 1],
    ["centos-uster-ch", "centos uster group", 6, 1, 2],
    ["centos-sion-ch", "centos sion dev", 6, 1, 3]
]

@locations = ["Goodison Park", "Craven Cottage", "Iduna Park", "Tella Parken"]
@address = ["Avenue", "Boulevard", "Street", "Road"]
@counter = 1
organization_list.each do |name, public_name, group_organization_id, type_id, pricing_policy_id|
    Organization.create!(name: name, public_name: public_name,
        group_organization_id: group_organization_id,
        type_id: type_id,
        pricing_policy_id: pricing_policy_id)

    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @counter)
    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @counter)
    @counter = @counter + 1
end

@counter = 1
@sub_org = ["north", "south", "east", "west"]
@sub_counter = 31
organization_list.each do |name, public_name, group_organization_id, type_id, pricing_policy_id|
    Organization.create!(name: name+"-"+@sub_org[0],
    public_name: public_name+" "+@sub_org[0], group_organization_id: group_organization_id,
    parent_id: @counter, type_id: type_id,
    pricing_policy_id: pricing_policy_id)

    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    @sub_counter = @sub_counter + 1

    Organization.create!(name: name+"-"+@sub_org[1],
        public_name: public_name+" "+@sub_org[1], group_organization_id: group_organization_id,
        parent_id: @counter, type_id: type_id,
        pricing_policy_id: pricing_policy_id)

    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    @sub_counter = @sub_counter + 1

    Organization.create!(name: name+"-"+@sub_org[2],
        public_name: public_name+" "+@sub_org[2], group_organization_id: group_organization_id,
        parent_id: @counter, type_id: type_id,
        pricing_policy_id: pricing_policy_id)

    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    @sub_counter = @sub_counter + 1

    Organization.create!(name: name+"-"+@sub_org[3],
        public_name: public_name+" "+@sub_org[3], group_organization_id: group_organization_id,
        parent_id: @counter, type_id: type_id,
        pricing_policy_id: pricing_policy_id)

    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    Location.create!(name: @locations[rand(0..3)], address: @locations[rand(0..3)]+" "+@address[rand(0..3)], organization_id: @sub_counter)
    @sub_counter = @sub_counter + 1

    @counter = @counter + 1
end